package br.fatec.projengsoft.principal.chamado;

/**
 *
 * @author BrenoXDMoon
 */
public enum StatusChamado {
    ABERTO("Aberto"),
    CANCELADO("Cancelado"),
    EM_ANDAMENTO("Em Andamento");
    
    private String descricao;

    private StatusChamado(String descricao) {
        this.descricao = descricao;
    }
    
    public String getDescricao(){
        return descricao;
    }  
}
