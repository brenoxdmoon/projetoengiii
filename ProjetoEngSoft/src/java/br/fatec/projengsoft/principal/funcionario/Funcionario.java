package br.fatec.projengsoft.principal.funcionario;

import br.fatec.projengsoft.principal.cargo.Cargo;
import br.fatec.projengsoft.principal.dominio.EntidadeDominio;

/**
 *
 * @author BrenoXDMoon
 */
public class Funcionario extends EntidadeDominio{
    
    private Long codigoIdentificacao;
    private String senha;
    private Cargo cargo;
}
