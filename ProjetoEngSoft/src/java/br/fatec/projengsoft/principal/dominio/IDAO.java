/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.fatec.projengsoft.principal.dominio;

import java.util.List;

/**
 *
 * @author BrenoXDMoon
 */
public interface IDAO {
    void salvar(EntidadeDominio obj);
    void alterar(EntidadeDominio obj);
    void deletar(EntidadeDominio obj);
    List<EntidadeDominio> listar(EntidadeDominio obj);

}
